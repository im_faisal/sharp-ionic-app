# README #

#### The following should be installed in your system to run this application: ####

* node
* npm
* Angular
* ionic

#### To run the application, clone the repo and do the following steps: ####

* npm install
* ionic cordova build ios
* ionic cordova run ios

#### Deployment instructions ####

* npm install
* ionic cordova build ios
* ionic cordova platform add ios
* then open the *app-name*.xcodeproj file in Xcode 
* verify and add 
    * <key>NSPhotoLibraryUsageDescription</key> <string>Photo Library Access Warning</string>.
    * <key>NSCameraUsageDescription</key><string>Camera usage description</string>
* increase the version no.
* archive the build
* upload for release

and generate the archive to upload

### What is this repository for? ###

* Quick summary: contains the camera upload app for Sharp Inc

* Version
    * In beta testing  -- 1.0(7)
    * In apple review  -- 1.0(7)
    * released         -- 


### TODO ###

* Writing tests
* Compose documentation
* New Features

### Who do I talk to? ###

* Faisal Ali (afaisal3389@gmail.com)