import { Component } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';
import { UserProvider } from './../../providers/user/user.service';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {


  private username: string;
  private password: string;
  private error: string;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public modalCtrl: ModalController,
    private userService: UserProvider
  ) {}

  ionViewDidLoad() {

  }

  login() {
    this.userService.login(this.username, this.password).subscribe(res => {
      if(res) {
        console.log('login succesfull');
      }
      return;
    },err => {
      console.log('error in login', err);
    })
    
  }
}
