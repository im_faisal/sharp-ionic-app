import { Component } from '@angular/core';
import { NavController, IonicPage, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { File } from '@ionic-native/file';
import { Platform } from 'ionic-angular';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { UpdateLogsProvider } from './../../providers/update-logs/update-logs.service';

require('aws-sdk/dist/aws-sdk');

/**
 * Generated class for the UploadImagePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage({
  name: 'upload-image'
})
@Component({
  selector: 'page-upload-image',
  templateUrl: 'upload-image.html',
})
export class UploadImagePage {
  orderId: string = '';
  trackingId: string = '';
  showUploadButton = false;
  showTakeImageButton = false;
  showReset: boolean = false;
  showProgressBar: boolean = false;

  AWSservice;
  bucket;
  imageBody;
  loadProgress: number = 0;
  base64ImageData;
  
  imageURI: any;
  imagePath: any;
  imagePreview: any;
  imageNameAfterUpload = '';
  localPath;
  localFileName;

  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    private transfer: FileTransfer,
    private camera: Camera,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    private file: File,
    public platform: Platform,
    public alertCtrl: AlertController,
    public updateLogService: UpdateLogsProvider
  ) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad UploadImagePage');
  }

  /**
   * function to get Image from Photo Library.
   */
  getImage() {
    if (!this.orderId || !this.trackingId) {
      this.presentToast('Please provide both OrderId and TrackingId');
      return;
    }
    this.showProgressBar = true;
    this.loadProgress = 0;
    const options: CameraOptions = {
      quality: 100,
      destinationType: (this.platform.is('ios'))?this.camera.DestinationType.FILE_URI:this.camera.DestinationType.NATIVE_URI,
      sourceType: this.camera.PictureSourceType.CAMERA,
      saveToPhotoAlbum: false      
    }
    
    this.camera.getPicture(options).then((imageData) => {

      // this.imagePreview = imageData;
      this.showReset = true;
      this.showUploadButton = true;
      
      this.localPath = "file://"+imageData.substring(7,imageData.lastIndexOf("/")); 
      this.localFileName = imageData.substring(imageData.lastIndexOf("/") + 1);
      
      console.log("saving to..");
      console.log(this.localPath);
      console.log("saving as file name..");
      console.log(this.localFileName);
      
      this.file.readAsDataURL(this.localPath, this.localFileName).then(result => {
        console.log("base64ImageData");
        this.base64ImageData = result;
      }, (err) => {
        console.log("err");
        console.log(err);
      })
      
      this.presentToast(imageData);
    }, (err) => {
      console.log(err);
      this.presentToast(err);
    });
  }

  getFileFromDevice(fileInput: any) {
      this.showReset = true;
      if (fileInput.target) {
        this.showUploadButton = true;        
        this.imageURI = fileInput.target.files[0].name;     //uploaded directly from computer
        this.imageBody = fileInput.target.files[0];    
      }
  }

  /**
   * function to upload the image files to the server.
   */
  uploadFile() {
    console.log("uploading...")
    this.showUploadButton = false;        
    
    // aws config
    this.AWSservice = (<any>window).AWS;
    this.AWSservice.config.accessKeyId = 'AKIAI74BJTORX7Z55D7Q';
    this.AWSservice.config.secretAccessKey = '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';
    this.bucket = new this.AWSservice.S3({params: {Bucket : 'sharp.test.img.2'}});


    let loader = this.loadingCtrl.create({
      content: `Uploading...`,
      dismissOnPageChange: false,
      showBackdrop: false
    });
    // loader.present();
    var d = new Date();
    var n = d.toISOString();
    let name = this.trackingId ? this.trackingId : 'trackingId';
    this.imageNameAfterUpload = 'sharp-' + n + name + '.jpeg';

    let body;
    if (this.base64ImageData) {

      // Split the base64 string in data and contentType
      var block = this.base64ImageData.split(";");
      // Get the content type of the image
      var contentType = block[0].split(":")[1];// In this case "image/gif"
      // get the real base64 content of the file
      var realData = block[1].split(",")[1];// In this case "R0lGODlhPQBEAPeoAJosM...."
      
      // Convert it to a blob to upload
      body = this.b64toBlob(realData, contentType, "");
    } else {
      
      body = this.imageBody;
    }

    let params = {Key : this.imageNameAfterUpload, ContentType: 'image/jpeg', Body : body};
    var _self = this;

    this.bucket.upload(params).on('httpUploadProgress', function(evt) {
      _self.loadProgress = Number(((evt.loaded * 100) / evt.total).toFixed(0));
    }).send(function(err, response){
      if (err) {
        console.log(err);
        _self.presentToast(err);
        _self.showReset = true;
        _self.showUploadButton = true;
      } else if (response) {
        // TODO: replace the image name with actual img-url
        _self.updateLogs(_self.orderId, _self.trackingId, response.Location);
        console.log(response.Location);
        _self.loadProgress = 100;
        // _self.presentToast("Image uploaded successfully");
        _self.presentAlert();

        //clear image form preview and read mode
        _self.imageURI = '';
        _self.imageBody = '';
  
        console.log('clearing image from preview');
        // _self.imagePreview = '';
        _self.imagePreview = response.Location;
        
        _self.showReset = false;

        console.log("removing the file from device");
        _self.removeFile(_self.localPath, _self.localFileName);
      }

    })

  }

  updateLogs(orderId, trackingId, imageName) {
    this.updateLogService.updateLogs(orderId, trackingId, imageName).subscribe(res => {
      console.log('logs updated succesfully');
      console.log(res);
    }, err => {
      console.log('error occured while updating logs');      
      console.log(err);
    })
  }

  removeFile(path, fileName) {

    console.log("removing from..");
    console.log(path);
    console.log("removing file name..");
    console.log(fileName);


    this.file.removeFile(path, fileName).then(result => {
      console.log("successfully removed file from device");
      console.log(result);
    }, (err) => {
      console.log("err in removing file");
      console.log(err);
    })
  }

  reset() {
    this.imageBody = '';
    this.imageURI = '';
    this.base64ImageData = '';
    this.imagePreview = '';
    this.showReset = false;
    this.showUploadButton = false;
    console.log('reset complete');
  }

  /**
   *  Toast Controller for display any error message.
   */
  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom'
    });
  
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
  
    toast.present();
  }

  presentAlert() {
    let alert = this.alertCtrl.create({
      title: 'Image Upload Successfull',
      subTitle: this.imageNameAfterUpload + ' image is uploaded succesfull to S3 bucket',
      buttons: ['Ok']
    });
    alert.present();
  }

  ionViewDidLeave() {
    // this.loader.dismiss();    
  }

  /**
   * to convert b63 file fomat to image blob
   * @param b64Data 
   * @param contentType 
   * @param sliceSize 
   */
  b64toBlob(b64Data, contentType, sliceSize) {
    contentType = contentType || '';
    sliceSize = sliceSize || 512;
  
    var byteCharacters = atob(b64Data);
    var byteArrays = [];
  
    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      var slice = byteCharacters.slice(offset, offset + sliceSize);
    
      var byteNumbers = new Array(slice.length);
      for (var i = 0; i < slice.length; i++) {
          byteNumbers[i] = slice.charCodeAt(i);
      }
    
      var byteArray = new Uint8Array(byteNumbers);
    
      byteArrays.push(byteArray);
    }
  
    var blob = new Blob(byteArrays, {type: contentType});
    return blob;
  }

}
