import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UploadImagePage } from './upload-image';

//mobile native APIs
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { Camera } from '@ionic-native/camera';
import { ProgressBarModule } from "angular-progress-bar";

// import { UpdateLogsProvider } from '../../providers/update-logs/update-logs.service';
import { HttpModule } from '@angular/http';

@NgModule({
  declarations: [
    UploadImagePage,
  ],
  imports: [
    IonicPageModule.forChild(UploadImagePage),
    ProgressBarModule,
    HttpModule
  ],
  providers: [
    FileTransfer,
    FileTransferObject,
    File,
    Camera,
    // UpdateLogsProvider,
  ]
})
export class UploadImagePageModule {}
