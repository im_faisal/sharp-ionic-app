import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { Platform } from 'ionic-angular';
import { File } from '@ionic-native/file';
import { AlertController } from 'ionic-angular';
import { GlobalConfig } from './../../config';

require('aws-sdk/dist/aws-sdk');

@Component({
  selector: 'page-list',
  templateUrl: 'list.html'
})
export class ListPage {

  sampleImage1 = 'https://static.pexels.com/photos/248797/pexels-photo-248797.jpeg';
  sampleImage2 = 'https://s3.ap-south-1.amazonaws.com/haeal.com.orders/10000-1498302839966/1498315928730image.jpg';
  orderImages = [this.sampleImage2, this.sampleImage1];
  base64ImageData;
  orderDetails;
  AWSservice;
  bucket;
  
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private camera: Camera,
    public platform: Platform,
    private file: File,
    private alertCtrl: AlertController
  ) {
    // aws config
    this.AWSservice = (<any>window).AWS;
    this.AWSservice.config.accessKeyId = GlobalConfig.awsConfig.accessKeyId;
    this.AWSservice.config.secretAccessKey = GlobalConfig.awsConfig.secretAccessKey;
    this.bucket = new this.AWSservice.S3({params: {Bucket : GlobalConfig.awsConfig.ordersBucket}});
  }

  ionViewDidLoad() {
    this.orderDetails = this.navParams.data.orderDetails;

    var params = {Bucket: 'sharp.test.img.2', Prefix: 'Abalam 30s'};
    let self = this;
    this.bucket.listObjectsV2(params, function(err, data){
      var bucketContents = data.Contents;
      console.log('data', data);
        for (var i = 0; i < bucketContents.length; i++){
          var urlParams = {Bucket: 'sharp.test.img.2', Key: bucketContents[i].Key};
          self.bucket.getSignedUrl('getObject',urlParams, function(err, url){
              console.log('the url of the image is', url);
            });
        }
    });
    
  }

  goBack() {
    this.navCtrl.pop();
  }

  addButtonClicked() {
    this.presentPopup();    
  }

  getImage(source) {
    const options: CameraOptions = {
      quality: 100,
      destinationType: (this.platform.is('ios'))?this.camera.DestinationType.FILE_URI:this.camera.DestinationType.NATIVE_URI,
      sourceType: source == 'camera' ? this.camera.PictureSourceType.CAMERA : this.camera.PictureSourceType.PHOTOLIBRARY,
      saveToPhotoAlbum: false      
    }

    this.camera.getPicture(options).then((imageData) => {
      
      let localPath = "file://"+imageData.substring(7,imageData.lastIndexOf("/")); 
      let localFileName = imageData.substring(imageData.lastIndexOf("/") + 1);
      
      this.file.readAsDataURL(localPath, localFileName).then(result => {
        console.log("base64ImageData");
        this.base64ImageData = result;
      }, (err) => {
        console.log(err);
      })
    }, (err) => {
      console.log(err);
    });
  }

  presentPopup() {
    let alert = this.alertCtrl.create({
      title: 'Image upload',
      message: 'Where do you want to get image from?',
      buttons: [
        {
          text: 'Camera',
          role: 'camera',
          handler: () => {
            console.log('Camera clicked');
            // this.getImage();
          }
        },
        {
          text: 'Gallery',
          handler: () => {
            console.log('Gallery clicked');
            // this.getImage();
          }
        }
      ]
    });
    alert.present();
  }
}
