import { Component } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';
import { ListPage } from './../list/list';
import { OrdersProvider } from './../../providers/orders/orders.service';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  selectedItem: any;
  icons: string[];
  items: Array<{title: string, note: string, orderId: string, isImagePresent: boolean, orderNum: Number}>;
  showLoadingText: boolean;
  orders = [];
  tempOrders = [];

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public orderService: OrdersProvider
  ) {
    // this.items = [];
    // for (let i = 1; i < 21; i++) {
    //   this.items.push({
    //     title: 'Item ' + i,
    //     note: 'This is item #' + i,
    //     orderId: 'OrderId' + i,
    //     isImagePresent: (i%3 == 0) ? false : true,
    //     orderNum: i
    //   });
    // }
  }

  ionViewDidLoad() {

    this.showLoadingText = true;
    //use getRecentOrders API once CORS issue is fixed.
    // this.orderService.getOrdersDummyData().subscribe(res => {
    this.orderService.getRecentOrders().subscribe(res => {
      if(res) {
        this.orders = res.data;
        this.tempOrders = this.orders.slice(0,100);
        this.showLoadingText = false;
      }
    },
    err => {
      console.log(err);
    })
  }

  itemTapped(event, order) {
    this.presentProfileModal(order);
  }

  presentProfileModal(order) {
    let profileModal = this.modalCtrl.create(ListPage, { orderDetails: order });
    profileModal.present();
  }
}
