import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { Headers, RequestOptions } from '@angular/http';
import { GlobalConfig } from './../../config';

/*
  Generated class for the UpdateLogsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UpdateLogsProvider {

  constructor(public http: Http) {
  }

  /**
   * api to update business login after the upload of image
   * @param orderId 
   * @param transactionId 
   * @param uploadedImageName 
   * TODO:: use base url from config
   */
  updateLogs(orderId, trackingId, uploadedImageName) {

    var url = GlobalConfig.baseUrl + 'v2/tRetailEx/s3updatelogs';
    var body = JSON.stringify({
      id: orderId,
      tracking: trackingId,
      imagepath: uploadedImageName
    });
    let	options = this.getHeaders();
    return this.http.post(url, body, options);
  }

  /**
   * utility function to get Header for API call
   */
  getHeaders() {
	  let headers = new Headers({
        'Content-Type': 'application/json'
    });
    let options = new RequestOptions({ headers: headers });
    return options;
  }

}
