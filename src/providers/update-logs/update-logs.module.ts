import {
    NgModule,
    ModuleWithProviders
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { Http } from '@angular/http';

import { UpdateLogsProvider } from './update-logs.service';

export function updateLogsProviderFactory( http: Http ){
    return new UpdateLogsProvider(http)
}

@NgModule({
    imports: [
        CommonModule,
        Http
    ]
})
export class UpdateLogsModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: UpdateLogsModule,
            providers: [{
                provide: UpdateLogsProvider, useFactory: updateLogsProviderFactory, 
                deps:[]
            }]
        };
    }
}