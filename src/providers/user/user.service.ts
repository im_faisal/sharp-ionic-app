import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { Headers, RequestOptions } from '@angular/http';
import { GlobalConfig } from './../../config';

/*
  Generated class for the UpdateLogsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UserProvider {

  constructor(public http: Http) {
  }

  /**
   * api to login
   * @param username 
   * @param password 
   */
  login(username, password) {
    
    var url = GlobalConfig.baseUrl + 'mysql/users/login';
    var body = JSON.stringify({
      username: username,
      password: password
    });
    let	options = this.getHeaders();
    return this.http.post(url, body, options);
  }

  /**
   * utility function to get Header for API call
   */
  getHeaders() {
	  let headers = new Headers({
        'Content-Type': 'application/json'
    });
    let options = new RequestOptions({ headers: headers });
    return options;
  }

}
