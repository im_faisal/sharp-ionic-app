import {
    NgModule,
    ModuleWithProviders
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { Http } from '@angular/http';

import { UserProvider } from './user.service';

export function userProviderFactory( http: Http ){
    return new UserProvider(http)
}

@NgModule({
    imports: [
        CommonModule,
        Http
    ]
})
export class UserModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: UserModule,
            providers: [{
                provide: UserProvider, useFactory: userProviderFactory, 
                deps:[]
            }]
        };
    }
}