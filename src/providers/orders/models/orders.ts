
export class Orders {
  
    allClients: any;
    allItems: any;
    data: any;
    locations: any;
    constructor(obj) {

        this.allClients = obj.allClients || [];
        this.allItems = obj.allItems || [];
        this.data = obj.data || [];
        this.locations = obj.locations || [];
    }
}