import {
    NgModule,
    ModuleWithProviders
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { Http } from '@angular/http';

import { OrdersProvider } from './orders.service';

export function ordersProviderFactory( http: Http ){
    return new OrdersProvider(http)
}

@NgModule({
    imports: [
        CommonModule,
        Http
    ]
})
export class OrdersModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: OrdersModule,
            providers: [{
                provide: OrdersProvider, useFactory: ordersProviderFactory, 
                deps:[]
            }]
        };
    }
}